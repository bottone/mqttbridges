import java.io.*; 
import java.net.*;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.concurrent.LinkedBlockingQueue;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttSecurityException;

/* 
 * A TCP server listening to MQTT messages published to MQTT_BROKER under
 * topic MQTT_TOPIC.
 * All the messages are stored in a queue.
 * 
 * Clients connecting to the TCP server can retrieve and delete all messages
 * received, or post new MQTT messages (any topic):
 * - to retrieve and delete, use getMessages
 * - to post, use "publish topic message"
 *  
 *  You can test using the command line with
 *  $ telnet localhost TCP_PORT
 */
public class MQTTStore implements MqttCallback {

	ServerSocket incomingSocket;

	private MqttClient mQTTClient;

	// Incoming MQTT messages are stored here
	LinkedBlockingQueue<String> mQTTMessages = null;

	/* Some constants */

	private static int TCP_PORT = 6789;

	// Change the address of the broker as appropriate. I use
	// mosquitto for a local borker, otherwise use broker.hivemq.com 
	// (they also have a nice web client)	
	//		private static String BROKER_ADDRESS = "tcp://localhost:1883";	
	private static String BROKER_ADDRESS = "tcp://broker.hivemq.com:1883";	

	// The name of this client (used when connecting to the broker)
	// Notice that each client should have a different name
	private static String CLIENT_NAME = "MASE-TCPMQTTStore";

	// The topic we are interested in.
	private static String SUBSCRIBED_TOPIC = "SimpleMQTTTest";

	/* End of constants */



	public MQTTStore() {
		try {
			incomingSocket = new ServerSocket(TCP_PORT);
		} catch (IOException e) {
			System.err.println("MQTTStore: Oops, I could not set up a socket in the constructor!");
			e.printStackTrace();
			// No point in continuing if we cannot set up a socket
			System.exit(1);
		}

		// Setting up MQTT client:
		// Connect to broker:
		try {
			mQTTClient = new MqttClient(BROKER_ADDRESS, CLIENT_NAME);
			mQTTClient.connect();
		} catch (MqttSecurityException e) {
			System.err.println("MQTTStore: Oops, I could not connect to the MQTT broker for a security exception! ");
			e.printStackTrace();
			// No point in continuing if we cannot set up a MQTT connection
			System.exit(1);
		} catch (MqttException e) {
			System.err.println("MQTTStore: Oops, I could not connect to the MQTT broker!");
			e.printStackTrace();
			// No point in continuing if we cannot set up a MQTT connection
			System.exit(1);		}

		// Set callback for subscriptions
		mQTTClient.setCallback(this);

		// # is the wildcard for multiple topics, this will match
		// SUBSCRIBED_TOPIC/
		// SUBSCRIBED_TOPIC/subtopic
		// SUBSCRIBED_TOPIC/subtopic/subsubtopic
		// etc.
		try {
			mQTTClient.subscribe(SUBSCRIBED_TOPIC+"/#");
		} catch (MqttException e) {
			System.err.println("MQTTStore: Oops, error when subscribing to MQTT topic!");
			e.printStackTrace();
		}

		// Finally, setting up the queue to store messages
		mQTTMessages = new LinkedBlockingQueue<>();
		System.out.println("MQTTStore: TCP server up and running");
	}

	/* 
	 * Main loop for TCP server.
	 * Notice: only one client can connect, this is on purpose.
	 */
	public void run() {
		Socket connectionSocket = null;	
		BufferedReader inFromClient = null;
		String messageFromClient = null;

		while(true) {

			try {	
				connectionSocket = incomingSocket.accept();
			} catch (IOException e) {
				System.out.println("MQTTStore: Oops, something wrong when a client was trying to connect");
				e.printStackTrace();
			}

			try {
				if ( connectionSocket != null ) {	
					inFromClient = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));	
				}
			} catch (IOException e) {
				System.out.println("MQTTStore: Oops, something wrong when reading data from client");
				e.printStackTrace();
			}

			while (connectionSocket.isConnected()) {

				try {
					messageFromClient = inFromClient.readLine();
					if (messageFromClient == null ) {
						// This means that the client has disconnected.
						break;
					}
				} catch (IOException e1) {
					System.out.println("MQTTStore: Oops, something wrong when reading a line from client");
					e1.printStackTrace();
				}


				if ( messageFromClient != null ) {
					// we have a message, let's open an output stream to client
					DataOutputStream outToClient = null;
					try {
						outToClient = new DataOutputStream(connectionSocket.getOutputStream());
					} catch (IOException e) {
						// TODO Auto-generated catch block
						System.out.println("MQTTStore: ops, something wrong opening stream to client");
						e.printStackTrace();
					}				

					/* Crappy code ahead: only 2 message types are supported:
					 * 0) "getMessages" will return messages currently in the queue and return
					 * 1) "publish some/topic someMessage" will publish an MQTT message 
					 */
					if ( outToClient != null ) {
						if ( messageFromClient.startsWith("getMessages")) {
							System.out.println("DEBUG: getting messages");
							// A message to retrieve the content of the queue
							if (mQTTMessages.size() > 0 ) {
								// Just one thread is draining, so we are OK here
								LinkedList<String> currentMessages = new LinkedList<>();
								mQTTMessages.drainTo(currentMessages);
								String response = "";
								for (String msg: currentMessages) {
									// Quick fix: separate messages with semicolons
									// and put everything on a single line
									response = response+msg+";";
								}
								try {
									outToClient.writeBytes(response+"\n");
								} catch (IOException e) {
									System.out.println("MQTTStore: Oops, something wrong opening trying to write to client");
									e.printStackTrace();
								}
								
							}	
						} else if (messageFromClient.startsWith("publish")) {
							System.out.println("DEBUG: publishing message");
							// The incoming message should be: publish topic some message 
							// I assume no space in topic.
							String[] parts = messageFromClient.split(" ");
							try {
								String topic = parts[1];
								// Put together again whatever is after the topic:
								String fullMessage = String.join(" ",Arrays.copyOfRange(parts, 2, (parts.length)));
								MqttMessage mQTTMsg = new MqttMessage();
								mQTTMsg.setPayload(fullMessage.getBytes());
								try {
									mQTTClient.publish(topic,mQTTMsg);
								} catch (MqttException e) {
									System.out.println("MQTTStore: Oops, something wrong publishing MQTT message");
									e.printStackTrace();
								}
							} catch (ArrayIndexOutOfBoundsException e) {
								System.out.println("MQTTStore: Oops, something wrong managing the incoming messages to be published: "+messageFromClient);
								e.printStackTrace();
							}
						} else {
							System.out.println("MQTTStore: Oops, I did not understand this message!");
						}	
						System.out.println("DEBUG: done managing a message");
					}
				}
			}
			try {
				connectionSocket.close();
			} catch (IOException e) {
				System.out.println("MQTTStore: Oops, something wrong trying to close a connection");
				e.printStackTrace();
			}
			System.out.println("Someone has disconnected");
		}	
	}

	@Override
	public void connectionLost(Throwable arg0) {
		System.out.println("MQTTStore: lost connection to MQTT broker");

	}

	@Override
	public void deliveryComplete(IMqttDeliveryToken arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void messageArrived(String topic, MqttMessage message) throws Exception {
		mQTTMessages.add(topic+" "+new String(message.getPayload()));
	}

	public static void main (String [] args) {
		MQTTStore aStore = new MQTTStore();
		aStore.run();
	}

}
